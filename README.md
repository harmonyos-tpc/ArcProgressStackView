ArcProgressStackView
====================

Present your progress bars in arc mode with information and total control.

ScreenShot
====

<img src="https://gitee.com/HarmonyOS-tpc/ArcProgressStackView/raw/master/screenshot/1.gif" width="30%"> 
<img src="https://gitee.com/HarmonyOS-tpc/ArcProgressStackView/raw/master/screenshot/2.gif" width="30%">

How to import into your project
====
通过library生成har包，添加har包到libs文件夹内
在entry的gradle内添加如下代码
```gradle
implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
```

方法二：
```gradle
allprojects{
    repositories{
        mavenCentral()
    }
}
implementation 'io.openharmony.tpc.thirdlib:ArcProgressStackView:1.0.2'
```

Sample
======

<b>Parameters</b>

For `APSV` you can set such parameters as:

 - models:  
    allows you to set `APSV` models, where you set title, progress and colors. Can be set up only via code.

 - animation:  
    allows you to animate progress with func call or with touch event.

 - shadow:  
    allows you to add shadow to your progress models.

 - round corners:  
    allows you to set round corners to models.

 - drag/seek:  
    allows you to make your progress model seek like.

 - typeface:  
    allows you to set custom typeface.

 - text color:  
    allows you to set text(title and progress indicator) color.

 - shadow distance:  
    allows you to set shadow distance.

 - shadow angle:  
    allows you to set shadow angle.

 - shadow radius:  
     allows you to set shadow radius.

 - shadow color:  
     allows you to set shadow color.

 - animation duration:  
     allows you to set animation duration.

 - animation listener:  
     allows you to set animation listener.

 - interpolator:  
     allows you to set interpolator to animation.

 - draw width:  
     allows you to set draw width in fraction mode(e.g. 0.55) or dimension mode.

 - model offset:  
     allows you to set model offset(positive or negative).

 - model background:  
     allows you to set model background track from start angle to sweep angle.

 - start angle:  
     allows you to set start angle of models.

 - sweep angle:  
     allows you to set sweep angle of models.

 - progress indicator orientation:  
     allows you to set progress indicator orientation.

 - preview colors:  
     allows you to set preview colors, which generate count of models equals to count of colors.

 - preview background:  
    allows you to set models background preview color.
    
<b>Tips</b>

All angle variables can only be positive and be in range from 0 to 360 degrees.  
The size of `Component` automatically set up in square mode.

<b>Init</b>

Check out in code init
```java
  final ArrayList<ArcProgressStackView.Model> models = new ArrayList<>();
        models.add(new ArcProgressStackView.Model("Strategy", 0, new Color(Color.getIntColor(bgColors[0])), new Color(mStartColors[0])));
        models.add(new ArcProgressStackView.Model("Design", 0, new Color(Color.getIntColor(bgColors[1])), new Color(mStartColors[1])));
        models.add(new ArcProgressStackView.Model("Development", 0, new Color(Color.getIntColor(bgColors[2])), new Color(mStartColors[2])));
        models.add(new ArcProgressStackView.Model("QA", 0, new Color(Color.getIntColor(bgColors[3])), new Color(mStartColors[3])));
        
mArcProgressStackView = (ArcProgressStackView) findComponentById(ResourceTable.Id_apsv);
mArcProgressStackView.setModels(models);
```

Other methods check out in sample.

And `XML` init:
```xml
   <devlight.io.library.ArcProgressStackView
            ohos:id="$+id:apsv"
            ohos:width="300vp"
            ohos:height="300vp"
            ohos:layout_alignment="horizontal_center"
            hap:apsv_model_bg_enabled="true"
            hap:apsv_rounded="true"
            hap:apsv_shadowed="true"
            hap:apsv_animated="true"
            hap:apsv_dragged="true"
            hap:apsv_leveled="true"
            hap:apsv_typeface="agency.ttf"
            hap:apsv_text_color="#fff"
            hap:apsv_shadow_distance="5vp"
            hap:apsv_shadow_angle="90"
            hap:apsv_shadow_radius="10vp"
            hap:apsv_shadow_color="#000"
            hap:apsv_animation_duration="1000"
            hap:apsv_interpolator="4"
            hap:apsv_draw_width="0.75"
            hap:apsv_model_offset="5vp"
            hap:apsv_start_angle="270"
            hap:apsv_sweep_angle="360"
            hap:apsv_indicator_orientation="1"
            hap:apsv_preview_bg="#ccc"/>
```

Author
======

Created by [Basil Miller](https://github.com/GIGAMOLE) - [@gigamole](mailto:gigamole53@gmail.com)


Created by [Basil Miller](https://github.com/GIGAMOLE) - [@gigamole](mailto:gigamole53@gmail.com)
=======
[Here](https://github.com/DevLight) you can see open source work developed by Devlight LLC.  
This and another works is an exclusive property of Devlight LLC. 

If you want to use this library in applications which will be available on Google Play, please report us or author of the library about it.

Whether you're searching for a new partner or trusted team for creating your new great product we are always ready to start work with you. 

You can contact us: info@devlight.io or opensource@devlight.io.  
Thanks in advance.

Devlight LLC, 2016  
[devlight.io](http://devlight.io)