/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package devlight.io.sample;

import com.gigamole.arcprogressstackview.ResourceTable;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Slider;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.agp.window.dialog.PopupDialog;
import ohos.app.Context;

/**
 * 颜色选择框
 */
public class ColorDialog extends PopupDialog implements Slider.ValueChangedListener {
    private Context mContext;
    private int red = 0;
    private int green = 0;
    private int blue = 0;
    private Component colorComponent;
    private ShapeElement shapeElement;
    private ConfirmListener listener;

    /**
     * 确认按钮的接口
     */
    public interface ConfirmListener {
        void onConfirm(Color color);
    }

    public ColorDialog(Context context, Component contentComponent) {
        super(context, contentComponent);
        mContext = context;
    }


    @Override
    protected void onCreate() {
        super.onCreate();
        DirectionalLayout component = (DirectionalLayout) LayoutScatter.getInstance(mContext)
                .parse(ResourceTable.Layout_dialog_color, null, false);
        setCustomComponent(component);
        Slider rSlider = (Slider) component.findComponentById(ResourceTable.Id_slider_r);
        Slider gSlider = (Slider) component.findComponentById(ResourceTable.Id_slider_g);
        Slider bSlider = (Slider) component.findComponentById(ResourceTable.Id_slider_b);
        colorComponent = component.findComponentById(ResourceTable.Id_component);
        Button ok = (Button) component.findComponentById(ResourceTable.Id_ok);
        Button cancel = (Button) component.findComponentById(ResourceTable.Id_cancle);
        rSlider.setValueChangedListener(this);
        gSlider.setValueChangedListener(this);
        bSlider.setValueChangedListener(this);
        shapeElement = new ShapeElement();
        shapeElement.setRgbColor(new RgbColor(red, green, blue));
        colorComponent.setBackground(shapeElement);
        ok.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                listener.onConfirm(new Color(Color.rgb(red, green, blue)));
            }
        });

        cancel.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                hide();
            }
        });
    }

    @Override
    public void onProgressUpdated(Slider slider, int progress, boolean fromUser) {
        switch (slider.getId()) {
            case ResourceTable.Id_slider_r:
                red = progress;
                shapeElement.setRgbColor(new RgbColor(red, green, blue));
                colorComponent.setBackground(shapeElement);
                break;
            case ResourceTable.Id_slider_g:
                green = progress;
                shapeElement.setRgbColor(new RgbColor(red, green, blue));
                colorComponent.setBackground(shapeElement);
                break;
            case ResourceTable.Id_slider_b:
                blue = progress;
                shapeElement.setRgbColor(new RgbColor(red, green, blue));
                colorComponent.setBackground(shapeElement);
                break;
        }
    }

    @Override
    public void onTouchStart(Slider slider) {
    }

    @Override
    public void onTouchEnd(Slider slider) {
    }

    /**
     * 监听确认按钮
     * @param listener ConfirmListener
     */
    public void setConfirmListener(ConfirmListener listener) {
        this.listener = listener;
    }
}
